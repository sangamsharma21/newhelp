import 'package:flutter/material.dart';

final kActiveCardColour = Colors.red[500];
final kInactiveCardColour = Color(0xffe3e3e3);

final kActiveTextColour = Colors.white;
final kInactiveTextColour = Colors.grey.shade400;
