import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class SendFeedback extends StatefulWidget {
  @override
  SendFeedbackWidget createState() => SendFeedbackWidget();
}

class SendFeedbackWidget extends State {
  double rating = 0;
  final feedback = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Feedback'),
        backgroundColor: Color(0xFF1B418D),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(
            bottom: 8.0, left: 10.0, right: 10.0, top: 8.0),
        child: RaisedButton(
          onPressed: () {
            print('Submitted');
          },
          child: Text(
            'Submit',
            style: TextStyle(
              color: Colors.white,
              fontSize: 15.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          color: Color(0xFF1B418D),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            alignment: Alignment.topCenter,
            fit: BoxFit.contain,
            scale: 1.0,
            colorFilter: ColorFilter.mode(
              Colors.white.withOpacity(0.1),
              BlendMode.dstATop,
            ),
            image: AssetImage(
              'images/feedback.png',
            ),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(
            left: 8.0,
            right: 8.0,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 5.0,
                        left: 15.0,
                        bottom: 5.0,
                      ),
                      child: Text(
                        'Please share your experience',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Color(0xFF1B418D),
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 5.0, left: 15.0, bottom: 5.0),
                      child: SmoothStarRating(
                        allowHalfRating: false,
                        onRated: (value) {
                          setState(
                            () {
                              rating = value;
                            },
                          );
                        },
                        starCount: 5,
                        rating: rating,
                        size: 40.0,
                        color: Color(0xFF1B418D),
                        borderColor: Color(0xFF1B418D),
                        spacing: 32.0,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 5.0, left: 15.0, bottom: 5.0),
                      child: Text(
                        'Additional Comments',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Color(0xFF1B418D),
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: SingleChildScrollView(
                                child: TextFormField(
                                  controller: feedback,
                                  autocorrect: true,
                                  maxLines: 5,
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.all(20),
                                    labelText: 'Feedback',
                                    border: OutlineInputBorder(),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xFF1B418D)),
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    labelStyle: TextStyle(
                                      color: Color(0xFF1B418D),
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
